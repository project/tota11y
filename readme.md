tota11y helps visualize how your site performs with assistive technologies.

tota11y is a single JavaScript file that inserts a small button in the bottom corner of your document.

The toolbar consists of several plugins that each provide their own functionality.

Many of these plugins "annotate" elements on the page. Sometimes to show their existence, other times to point out when something's wrong.

#Installation via Composer.

This method assumes your using Composer to manage your project (https://www.drupal.org/docs/develop/using-composer/using-composer-to-manage-drupal-site-dependencies#drupal-packagist).

Add the following snipet to your ```composer.json``` file.

```
"repositories": [
  {
    "type": "package",
    "package": {
      "name": "khan/tota11y",
      "version": "0.1.3",
      "type": "drupal-library",
      "dist": {
        "url": "https://github.com/Khan/tota11y/archive/0.1.3.zip",
        "type": "zip"
      },
      "require": {
          "composer/installers": "^1.2.0"
      }
    }
  }
]
```

Finally the following should be added as well:

```
"extra": {
  "installer-paths": {
      "libraries/{$name}": ["type:drupal-library"]
  }
}
```

#Installation via Download.

Download the latest tota11y release here - https://github.com/Khan/tota11y/releases

Place tota11y.min.js in /libraries/tota11y/dist/
